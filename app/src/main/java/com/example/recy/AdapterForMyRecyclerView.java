package com.example.recy;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.circularreveal.cardview.CircularRevealCardView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;


public class AdapterForMyRecyclerView extends RecyclerView.Adapter<AdapterForMyRecyclerView.MyViewHolder> {
    List<UserModel> myusermodel;
    Context context;
    Dialog mydialog;
    List<UserModel> listsearching;


    public AdapterForMyRecyclerView(List<UserModel> myusermodel, Context context) {
        this.myusermodel = myusermodel;
        this.context = context;
        this.listsearching=new ArrayList<>(myusermodel);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.items_recyclerview,parent,false);
        final MyViewHolder myholder=new MyViewHolder(view);
         mydialog=new Dialog(context);
         mydialog.setContentView(R.layout.show_items_recyclervuiew);
         mydialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
         myholder.linearLayout.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
         TextView tshowname =mydialog.findViewById(R.id.txt_show_name);
         TextView tshow_email=mydialog.findViewById(R.id.txt_show_emaile);
         CircleImageView civshowpic=mydialog.findViewById(R.id.showciv);
         tshowname.setText(myusermodel.get(myholder.getAdapterPosition()).getName());
         tshow_email.setText(myusermodel.get(myholder.getAdapterPosition()).getEmaile());
         civshowpic.setImageResource(myusermodel.get(myholder.getAdapterPosition()).getImage());
         mydialog.show();
             }
         });
        return myholder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tname.setText(myusermodel.get(position).getName());
        holder.temaeil.setText(myusermodel.get(position).getEmaile().substring(0,11)+"...");
        holder.myimageciv.setImageResource(myusermodel.get(position).getImage());


    }

    @Override
    public int getItemCount() {
        return myusermodel.size();
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView myimageciv;
        LinearLayout linearLayout;
        TextView tname;
        TextView temaeil;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            myimageciv = itemView.findViewById(R.id.imgcir);
            tname = itemView.findViewById(R.id.txtname);
            temaeil = itemView.findViewById(R.id.txtemaile);
            linearLayout=itemView.findViewById(R.id.liner);


        }
    }
}
