package com.example.recy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RecyclerViewClickInterface {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    public List<UserModel> userModels = new ArrayList<>();
    private AdapterForMyRecyclerView adapter;
    private FloatingActionButton flothing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        additems();
        recy();
        toolbar=findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getActionBar().setTitle("RecyclerView app");
        flothing = findViewById(R.id.btnadd);

        flothing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showpopup();
            }
        });


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    UserModel deleteItems ;
    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT |
            ItemTouchHelper.UP|ItemTouchHelper.DOWN) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            int firstpotion=viewHolder.getAdapterPosition();
            int endposition =target.getAdapterPosition();
            Collections.swap(userModels,firstpotion,endposition);
           adapter.notifyItemMoved(firstpotion,endposition);

            return true;
        }

        @Override
        public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int direction) {

            final int position =viewHolder.getAdapterPosition();
            final UserModel user = userModels.get(position);

            switch (direction) {

                case ItemTouchHelper.RIGHT:
                    deleteItems=userModels.get(position);
                    userModels.remove(position);
                    adapter.notifyItemRemoved(position);
                 Snackbar.make(recyclerView,"Remove Items", Snackbar.LENGTH_LONG).setAction("Undo", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            userModels.add(position,deleteItems);
                            adapter.notifyItemInserted(position);
                            recyclerView.scrollToPosition(position);

                        }
                    }).show();

                    break;
            }


        }

        @Override

        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {


            new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent))

                     .addSwipeRightActionIcon(R.drawable.ic_delete)
                    .create()
                    .decorate();


            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };

    private void showpopup() {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.add_torecyclerview, null);

        final EditText ename = v.findViewById(R.id.ed_givename);
        final EditText eemaile = v.findViewById(R.id.ed_giveemaile);
        Button btnadd = v.findViewById(R.id.btn_addtorecy);
        Button btncancel = v.findViewById(R.id.btn_cancell);
        builder.setView(v);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = ename.getText().toString();
                String emaile = eemaile.getText().toString();
                if (name.equals("")) {
                    ename.setError("Please Fill in the field");
                    return;
                }
                if (emaile.equals("")) {
                    eemaile.setError("Please Fill in the field");
                    return;
                }

                if (eemaile.length()<=11){
                    eemaile.setError("your character must more than 11");
                    return;

                }
                userModels.add(new UserModel(R.drawable.dn, name, emaile));
                adapter.notifyDataSetChanged();
                alertDialog.dismiss();
            }

        });
    }

    private void recy() {
        recyclerView = findViewById(R.id.recyclerview1);
        adapter = new AdapterForMyRecyclerView(userModels, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        RecyclerView.ItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);
    }

    private void additems() {
        userModels.add(new UserModel(R.drawable.m1, "Ali", "ali@yahoo.com"));
        userModels.add(new UserModel(R.drawable.man, "Mozhdeh", "mozhdeh@yahoo.com"));
        userModels.add(new UserModel(R.drawable.m4, "Saeed", "saeed@yahoo.com"));
        userModels.add(new UserModel(R.drawable.ww1, "Ghazal", "ghazal@yahoo.com"));
        userModels.add(new UserModel(R.drawable.w2, "Shadi", "shadi@yahoo.com"));
        userModels.add(new UserModel(R.drawable.m5, "Farhad", "farhad@yahoo.com"));
        userModels.add(new UserModel(R.drawable.ww2, "Nafiseh", "nafiseh@yahoo.com"));
        userModels.add(new UserModel(R.drawable.w4, "Saeedeh", "saeedeh@yahoo.com"));



    }
}
