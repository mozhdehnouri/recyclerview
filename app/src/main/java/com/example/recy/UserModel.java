package com.example.recy;

public class UserModel {
    public int image;
    public String name;
    public String emaile;

    ///getter&setter

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmaile() {
        return emaile;
    }

    public void setEmaile(String emaile) {
        this.emaile = emaile;
    }
    ////constructor

    public UserModel(int image, String name, String emaile) {
        this.image = image;
        this.name = name;
        this.emaile = emaile;
    }
}
